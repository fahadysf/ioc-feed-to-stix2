FROM python:3.10-slim

RUN pip install fastapi uvicorn poetry wheel

EXPOSE 8000

# run this before copying requirements for cache efficiency
RUN pip install --upgrade pip

RUN mkdir /ioc-feed-to-stix2
WORKDIR /ioc-feed-to-stix2
COPY requirements.txt .
RUN echo $(pwd)
RUN pip install -r requirements.txt
COPY . .

ENV UVICORN_PORT 8000
ENV UVICORN_HOST "0.0.0.0"

CMD ["uvicorn", "server:app"]