import logging
import pickle
import os
from stix2 import Indicator
from stix2 import Bundle

# Try importing the config
try:
    import config
except Exception as e:
    print("Config file missing or invalid: " + str(e))
    exit(1)

""" Serves URLs converting Autofocus IOC feeds
    to corresponding STIX bundle feeds.
"""
# Setup data and logging dirs
for name in ["data", "logs"]:
    if not os.path.exists(name):
        os.makedirs(name)

# Logging setup
if config.DEBUG:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO
logging.basicConfig(encoding='utf-8', level=log_level)


def convert_ip_feed(file: str) -> list:
    # Load Existing Indicators
    try:
        with open('data/ip_db.pickle', 'rb') as handle:
            ip_db = pickle.load(handle)
        assert type(ip_db) == dict
    except IOError:
        ip_db = dict()
    with open(file, 'r') as ipfile:
        iplist = ipfile.readlines()
    iplist = [i.strip() for i in iplist]
    ip_indicators = list()
    for i in iplist:
        if i in ip_db:
            indicator = ip_db[i]
        else:
            indicator = Indicator(name=f"IP-Indicator-{i}",
                                  pattern=f"[ipv4-addr:value = '{i}']",
                                  pattern_type="stix",
                                  spec_version="2.1")
            ip_db[i] = indicator
        ip_indicators.append(indicator)

    # Save IP Database
    try:
        with open('data/ip_db.pickle', 'wb') as handle:
            pickle.dump(ip_db, handle, protocol=pickle.HIGHEST_PROTOCOL)
    except Exception as e:
        logging.error(f"Error saving ip database. {str(e)}")

    return ip_indicators


def convert_domain_feed(file: str) -> list:
    # Load Existing Indicators
    try:
        with open('data/domain_db.pickle', 'rb') as handle:
            domain_db = pickle.load(handle)
        assert type(domain_db) == dict
    except IOError:
        domain_db = dict()

    with open(file, 'r') as domainfile:
        domain_list = domainfile.readlines()
    domain_list = [d.strip() for d in domain_list]
    domain_indicators = list()
    for i in domain_list:
        if i in domain_db:
            indicator = domain_db[i]
        else:
            indicator = Indicator(name=f"Domain-Indicator-{i}",
                                  pattern=f"[domain-name:value = '{i}']",
                                  pattern_type="stix",
                                  spec_version="2.1")
            domain_db[i] = indicator
        domain_indicators.append(indicator)
    # Save Domain Database
    try:
        with open('data/domain_db.pickle', 'wb') as handle:
            pickle.dump(domain_db, handle, protocol=pickle.HIGHEST_PROTOCOL)
    except Exception as e:
        logging.error(f"Error saving domain database. {str(e)}")

    return domain_indicators


def convert_hash_feed(file: str) -> list:
    # Load Existing Indicators
    try:
        with open('data/hash_db.pickle', 'rb') as handle:
            hash_db = pickle.load(handle)
        assert type(hash_db) == dict
    except IOError:
        hash_db = dict()

    with open(file, 'r') as hashfile:
        hash_list = hashfile.readlines()
    hash_list = [d.strip() for d in hash_list]
    hash_indicators = list()
    for i in hash_list:
        if i in hash_db:
            indicator = hash_db[i]
        else:
            indicator = Indicator(name=f"Hash-Indicator-{i}",
                                  pattern=f"[file:hashes.sha256 = '{i}']",
                                  pattern_type="stix",
                                  spec_version="2.1")
            hash_db[i] = indicator
        hash_indicators.append(indicator)
    # Save Hash Database
    try:
        with open('data/hash_db.pickle', 'wb') as handle:
            pickle.dump(hash_db, handle, protocol=pickle.HIGHEST_PROTOCOL)
    except Exception as e:
        logging.error(f"Error saving hash database. {str(e)}")

    return hash_indicators


def convert_to_stix_feed(file: str, name: str, type: str) -> Bundle:

    if type == 'ips':
        indicators = convert_ip_feed(file)
    elif type == 'domains':
        indicators = convert_domain_feed(file)
    elif type == 'hashes':
        indicators = convert_hash_feed(file)
    else:
        logging.error(f"Unsupported feed type: {type}")
    bundle = Bundle(indicators)
    return bundle
