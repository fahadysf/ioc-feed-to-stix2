import json
import logging
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from feedconverter import convert_to_stix_feed

# get root logger
logger = logging.getLogger(__name__)
app = FastAPI()


@app.get("/{feed_type}/{feed_name}")
async def feed(feed_type: str, feed_name: str):
    try:
        data = convert_to_stix_feed(
            f'./data/{feed_type}-{feed_name}.txt',
            f'{feed_name}',
            f'{feed_type}')
        return json.loads(data.serialize())
    except Exception as e:
        logger.error(str(e))
        return JSONResponse(
            {
                "error": "No data. Please check feed type and name.",
                "err_details": str(e),
            },
            status_code=404
        )
