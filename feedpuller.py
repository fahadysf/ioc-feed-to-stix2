import logging
import requests
import json
import datetime
import os
import sys
import config
import asyncio
from feedconverter import convert_to_stix_feed

# Setup data and logging dirs
for name in ["data", "logs", "output"]:
    if not os.path.exists(name):
        os.makedirs(name)

# -- Logging setup --
if config.DEBUG:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO
logging.basicConfig(encoding='utf-8', level=log_level)
log_formatter = logging.Formatter(
    "%(asctime)s [%(threadName)s] [%(levelname)s]  %(message)s")
file_handler = logging.FileHandler("logs/feedpuller.log")
console_handler = logging.StreamHandler()
file_handler.setFormatter(log_formatter)
file_handler.setLevel(log_level)
console_handler.setFormatter(log_formatter)
console_handler.setLevel(log_level)
logger = logging.getLogger(__name__)
logger.addHandler(file_handler)
logger.addHandler(console_handler)
# -- End of logging setup --


async def downloader_coroutine(feed: tuple):
    max_retries = 5
    retries = 0
    success = False
    while not success:
        try:
            r = requests.get(f"{feed[2]}", headers={
                "apikey": config.API_KEY}, timeout=config.TIMEOUT)
            if len(r.text) > 0:
                logger.info(
                    f"Successfully pulled feed. Name: {feed[0]} Type: {feed[1]}")
                with open(f'data/{feed[1]}-{feed[0]}.txt', 'w') as ofd:
                    ofd.write(r.text)
                with open(f'data/{feed[1]}-{feed[0]}.meta.json', 'w') as meta_ofd:
                    meta_ofd.write(json.dumps({
                        "feed_name": f"{feed[0]}",
                        "feed_type": f"{feed[1]}",
                        "feed_url": f"{feed[2]}",
                        "last_refreshed:": f'{datetime.datetime.now()}',
                    }, indent=2, sort_keys=False))
                success = True
        except Exception as e:
            logger.error(
                f"While pulling {feed[0]} (type: {feed[1]}, url: {feed[2]}):" + f"{str(e)}")
            retries += 1
            if retries == max_retries:
                return
            logger.info(
                f"[Attempt {retries+1}/{max_retries}] Retrying {feed[0]} (type: {feed[1]}, url: {feed[2]}) in 5 seconds")
            await asyncio.sleep(5)


async def main():
    tasks = list()
    for feed in config.FEEDS:
        tasks.append(asyncio.create_task(downloader_coroutine(feed)))
    for task in tasks:
        await task
    if '--convert' in sys.argv:
        for feed in config.FEEDS:
            data = convert_to_stix_feed(
                f'./data/{feed[1]}-{feed[0]}.txt',
                f'{feed[0]}',
                f'{feed[1]}')
            try:
                fname = f'output/{feed[1]}_{feed[0]}.stix2.json'
                with open(fname, 'w') as fd:
                    fd.write(data.serialize())
            except Exception as e:
                logger.error(f"Unable to write file: {fname}. Error: {str(e)}")
asyncio.run(main())
